package com.mobin.dao.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobin.dao.models.ProductPrice;
import com.mobin.dao.repositories.ProductPriceRepository;
import com.mobin.dao.services.ProductPriceService;
import com.mobin.utils.ConvertUtils;

@Service
public class ProductPriceServiceImpl implements ProductPriceService {

	ProductPriceRepository productPriceRepository;
	
	@Autowired
	public ProductPriceServiceImpl(ProductPriceRepository productPriceRepository) {
		// TODO Auto-generated constructor stub
		this.productPriceRepository = productPriceRepository;
	}
	
	@Override
	public Map<Integer, ProductPrice> getProductPriceMap(List<Integer> produtIds) {
		// TODO Auto-generated method stub
		return ConvertUtils.convertToMap(productPriceRepository.getProductPriceFromIds(produtIds), ProductPrice::getProductId);
	}

}
