package com.mobin.dao.services;

import com.mobin.dao.models.User;

public interface UserService {

	public User getUserOnUserName(String userName, String password);
}
