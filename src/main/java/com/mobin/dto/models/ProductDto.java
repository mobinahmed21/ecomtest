package com.mobin.dto.models;


import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ProductDto extends BaseDto {

	private String name;
	@JsonProperty("fullName")
	private String description;
	@JsonProperty("is_configurable")
	private Boolean configurable;
	private Integer quantity;
}
