package com.mobin.dao.services;

import java.util.List;
import java.util.Map;

import com.mobin.dao.models.ProductPrice;

public interface ProductPriceService {

	Map<Integer, ProductPrice> getProductPriceMap(List<Integer> produtIds);
}
