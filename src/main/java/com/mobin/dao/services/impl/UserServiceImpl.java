package com.mobin.dao.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobin.dao.models.User;
import com.mobin.dao.repositories.UserRepository;
import com.mobin.dao.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private final UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User getUserOnUserName(String userName, String password) {
		return userRepository.getUserFromUserName(userName, password);
	}

}
