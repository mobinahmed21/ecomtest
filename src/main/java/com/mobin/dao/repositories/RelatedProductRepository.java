package com.mobin.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.mobin.dao.models.RelatedProduct;

public interface RelatedProductRepository extends JpaRepository<RelatedProduct, Integer> {

	@Query(value = "SELECT relPr FROM RelatedProduct AS relPr WHERE relPr.parentProductId IN :parentProductIds")
	List<RelatedProduct> getRelatedProductsOnParentIds(@Param("parentProductIds") List<Integer> parentProductIds);
}
