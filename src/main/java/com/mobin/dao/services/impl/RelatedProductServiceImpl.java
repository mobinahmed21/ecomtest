package com.mobin.dao.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mobin.dao.models.RelatedProduct;
import com.mobin.dao.repositories.RelatedProductRepository;
import com.mobin.dao.services.RelatedProductService;
import com.mobin.utils.ConvertUtils;

@Service
public class RelatedProductServiceImpl implements RelatedProductService {

	RelatedProductRepository relatedProductRepository;
	
	public RelatedProductServiceImpl(RelatedProductRepository relatedProductRepository) {
		this.relatedProductRepository = relatedProductRepository;		
	}
	@Override
	public Map<Integer, List<RelatedProduct>> getRelatedProductsMapOnIds(List<Integer> parentIds) {
		// TODO Auto-generated method stub
		return ConvertUtils.convertToMapWithList(relatedProductRepository.getRelatedProductsOnParentIds(parentIds), RelatedProduct::getParentProductId);
	}

}
