package com.mobin.dto.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDto extends BaseDto {

	@JsonProperty("is_verified")
	private Boolean verified;
}
