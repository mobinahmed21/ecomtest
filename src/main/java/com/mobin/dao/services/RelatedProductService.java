package com.mobin.dao.services;

import java.util.List;
import java.util.Map;

import com.mobin.dao.models.RelatedProduct;

public interface RelatedProductService {

	Map<Integer, List<RelatedProduct>> getRelatedProductsMapOnIds(List<Integer> parentIds);
}
