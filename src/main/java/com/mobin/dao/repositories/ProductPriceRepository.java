package com.mobin.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mobin.dao.models.ProductPrice;

public interface ProductPriceRepository extends JpaRepository<ProductPrice, Integer>{
		
		@Query(value = "SELECT pr FROM ProductPrice AS pr WHERE pr.productId IN :productIds")
		List<ProductPrice> getProductPriceFromIds(@Param("productIds") List<Integer> productIds);
		
}
