package com.mobin.dao.models;

import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.mobin.dao.services.RelatedProductService;
import com.mobin.dto.mapper.DataMapper;
import com.mobin.dto.models.RelatedProductDto;

public class RelatedProductServiceTest extends BaseTest{

	@Autowired
	RelatedProductService relatedProductService;
	DataMapper dataMapper;
	
	@Before
	public void before(){
		Mapper mapper =  new DozerBeanMapper();
		dataMapper = new DataMapper(mapper, null);
	}
	
	@Test
	public void testRelatedProducts(){
		List<Integer> parentProductIds = Lists.newArrayList(1,2);
		Map<Integer, List<RelatedProduct>> relatedproductsMap = relatedProductService.getRelatedProductsMapOnIds(parentProductIds);
		for(Integer parentProductId:parentProductIds){
			List<RelatedProduct> relatedProducts = relatedproductsMap.get(parentProductId);
			for(RelatedProduct relatedProduct:relatedProducts){
				RelatedProductDto relatedProductDto = dataMapper.convertFromEntityToDto(relatedProduct, RelatedProductDto.class);
				Assert.assertEquals(relatedProductDto.getParentProductId(), parentProductId);
				System.out.println("PARENT PRODUCT ID:: " + relatedProductDto.getParentProductId());
				System.out.println("PRODUCT ID:: "+relatedProductDto.getProductId());
			}
		}
	}
}
