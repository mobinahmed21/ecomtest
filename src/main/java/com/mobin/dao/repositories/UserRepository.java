package com.mobin.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mobin.dao.models.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	

	@Query(value = "SELECT us FROM User AS us WHERE us.userName = :username and us.password = :password")
	public User getUserFromUserName(@Param("username")String userName, @Param("password")String password);

}
