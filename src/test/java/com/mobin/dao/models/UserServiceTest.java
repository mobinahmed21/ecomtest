package com.mobin.dao.models;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mobin.dao.services.UserService;
import com.mobin.dto.mapper.DataMapper;

public class UserServiceTest extends BaseTest {

	@Autowired
	UserService userService;
	DataMapper dataMapper;
	
	@Before
	public void before(){
		Mapper mapper =  new DozerBeanMapper();
		dataMapper = new DataMapper(mapper, null);
	}
	
	@Test
	public void testUsers(){
		User user = userService.getUserOnUserName("Mobin", "Test1");
		System.out.println("USERNAME:: "+user.getUserName());
	}
}
